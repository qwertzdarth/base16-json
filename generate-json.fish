#! /usr/bin/env fish

set -l dir (pwd)

set -l tmp (mktemp -d)
git clone https://github.com/chriskempson/base16-builder-php $tmp
cd $tmp

composer install -d $tmp
php builder.php update

mkdir $dir/schemes
for themePath in (find schemes/ -maxdepth 2 -regex '.*\.yaml')
  set -l name (string split -r -m1 . (basename $themePath))[1]
  yq . $themePath > $dir/schemes/$name.json
end

cd $dir
