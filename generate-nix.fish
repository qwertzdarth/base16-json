#! /usr/bin/env fish
begin
echo "with builtins; {"
for scheme in (find schemes/*.json | sed "s@schemes/@@" | sed "s@\.json\$@@")
  echo "  \"$scheme\" = fromJSON (readFile schemes/$scheme.json);"
end
echo "}"
end > default.nix

